<?php include 'includes/header.php';

$id = $_GET['id'];
$chave = array(
  'id_categoria' => $id
);
$categoria = $crudRead->ListarSingle('tbl_categorias', $chave);
?>
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Editar Categoria</h1>
    
    <form name="form" id="form" method="post" action="bd/update/editarCategoria.php">
    <input type="hidden" id="id" name="id" class="input-text" value="<?php echo $categoria[0]['id_categoria'];?>" />
    <div class="input-field">
        <label for="category-name" class="label">Nome Categoria</label>
        <input type="text" id="nome_categoria" name="nome_categoria" class="input-text" value="<?php echo $categoria[0]['nome_categoria'];?>" required />
        
      </div>
      <div class="input-field">
        <label for="category-code" class="label">Código Categoria</label>
        <input type="text" id="cod_categoria" name="cod_categoria" class="input-text" value="<?php echo $categoria[0]['cod_categoria'];?>" required />
        
      </div>
      <div class="actions-form">
        <a href="categorias.php" class="action back">Voltar</a>
        <input name="save" class="btn-submit btn-action"  type="submit" value="Editar Categoria" />
      </div>
    </form>
    <div id="resposta"></div>
  </main>
  <!-- Main Content -->

  <?php include 'includes/footer.php';?>
