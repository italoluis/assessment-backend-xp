<?php
include '../../classes/class.crud.php';
include '../../classes/class.strings.php';
include '../../classes/class.geraCaracteres.php';
include '../../classes/class.upload.php';


$obj  = new ConexaoBanco;
$crud = new Crud;
$slug = new TratarString;
$geraCod = new GeraCaracteres;

//validar se os campos estão vazios
if (empty($_POST['nome_produto'])) {
  echo "<script>Swal.fire({  
    icon: 'error',
    title: 'Nome do produto não pode estar vazio',
    showConfirmButton: false,
    timer: 1500
  })</script>";
  exit();
}
if (empty($_POST['sku_produto'])) {
  echo "<script>Swal.fire({  
    icon: 'error',
    title: 'SKU do produto não pode estar vazio',
    showConfirmButton: false,
    timer: 1500
  })</script>";
  exit();
}
if (empty($_POST['preco_produto'])) {
  echo "<script>Swal.fire({  
    icon: 'error',
    title: 'Preço do produto não pode estar vazio',
    showConfirmButton: false,
    timer: 1500
  })</script>";
  exit();
}
if (empty($_POST['descricao_produto'])) {
  echo "<script>Swal.fire({  
    icon: 'error',
    title: 'A descrição do produto não pode estar vazia',
    showConfirmButton: false,
    timer: 1500
  })</script>";
  exit();
}
if (empty($_POST['categoria_produto'])) {
  echo "<script>Swal.fire({  
    icon: 'error',
    title: 'Precisa selecionar pelo menos uma categoria',
    showConfirmButton: false,
    timer: 1500
  })</script>";
  exit();
}
if (empty($_FILES['imagem_produto']['name'])) {
  echo "<script>Swal.fire({  
    icon: 'error',
    title: 'Precisa informar uma imagem',
    showConfirmButton: false,
    timer: 1500
  })</script>";
  exit();
}

$ip = $_SERVER['REMOTE_ADDR'];

$imagem_produto = $geraCod->Gerar(12);
$arquivo  = $_FILES['imagem_produto'];
$caminho  = '../../upload/';
$novoNome = $imagem_produto;
$upload   = new Upload($arquivo, $caminho, $novoNome);
$tipo     = $upload->extensaoImg();
$imagem_produto = $novoNome . '.' . $tipo;

$nome_produto = $crud->EscapaSql(strip_tags($_POST['nome_produto']));
$slug_produto = $crud->EscapaSql($slug->Slug($nome_produto));
$sku_produto    = $crud->EscapaSql(strip_tags($_POST['sku_produto']));
$preco_produto  = $crud->EscapaSql(strip_tags($_POST['preco_produto']));
$descricao_produto  = $crud->EscapaSql(strip_tags($_POST['descricao_produto'],'<h1><h2><p><b><i>'));
$quantidade_produto = $crud->EscapaSql(strip_tags($_POST['quantidade_produto']));
$categoria_produto  = strip_tags(json_encode($_POST['categoria_produto']));

$campos = array(
  'nome_produto' => $nome_produto,
  'slug_produto' => $slug_produto,
  'imagem_produto' => $imagem_produto,
  'sku_produto'    => $sku_produto,
  'preco_produto'  => $preco_produto,
  'descricao_produto'  => $descricao_produto,
  'quantidade_produto' => $quantidade_produto,
  'categoria_produto'  => $categoria_produto
);

if ($crud->Insert('tbl_produtos', $campos) != true) {
  echo "<script>Swal.fire({  
    icon: 'error',
    title: 'Produto não Cadastrado',
    showConfirmButton: false,
    timer: 1500
  })</script>";
  //inserir log
  $campos_log = array(
    'tipo_acao'   => 'insert',
    'tbl_acao'    => 'tbl_produtos',
    'status_acao' => 'erro',
    'ip_usuario'  => $ip
  );
  $crud->Insert('tbl_logs', $campos_log);
  echo ' <script>setTimeout(function(){window.location.reload(true)}, 1650);</script>';
} else {
  echo "<script>Swal.fire({  
    icon: 'success',
    title: 'Produto Cadastrado',
    showConfirmButton: false,
    timer: 1500
  })</script>";

  //inserir log
  $campos_log = array(
    'tipo_acao'   => 'insert',
    'tbl_acao'    => 'tbl_produtos',
    'status_acao' => 'sucesso',
    'ip_usuario'  => $ip
  );
  $crud->Insert('tbl_logs', $campos_log);
  $upload->Send();
  echo ' <script>setTimeout(function(){window.location.reload(true)}, 1650);</script>';
}
