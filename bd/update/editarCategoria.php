<?php
include '../../classes/class.crud.php';
include '../../classes/class.strings.php';
include '../../classes/class.geraCaracteres.php';
include '../../classes/class.upload.php';

$obj  = new ConexaoBanco;
$crud = new Crud;
$slug = new TratarString;

$ip = $_SERVER['REMOTE_ADDR'];

//validar se os campos estão vazios
if (empty($_POST['id'])) {
  echo "<script>Swal.fire({  
    icon: 'error',
    title: 'ID da categoria não informado',
    showConfirmButton: false,
    timer: 1500
  })</script>";
  exit();
}
if (empty($_POST['cod_categoria'])) {
  echo "<script>Swal.fire({  
    icon: 'error',
    title: 'Código não pode estar vazio',
    showConfirmButton: false,
    timer: 1500
  })</script>";
  exit();
}
if (empty($_POST['nome_categoria'])) {
  echo "<script>Swal.fire({  
    icon: 'error',
    title: 'Nome da categoria não pode estar vazio',
    showConfirmButton: false,
    timer: 1500
  })</script>";
  exit();
}


$id = $crud->EscapaSql($_POST['id']);
$chave = array(
  'id_categoria' => $id
);

$cod_categoria  = $crud->EscapaSql(strip_tags($_POST['cod_categoria']));
$nome_categoria = $crud->EscapaSql(strip_tags($_POST['nome_categoria']));
$slug_categoria = $crud->EscapaSql(strip_tags($slug->Slug($nome_categoria)));

//Testar se a categoria esta ligada a algum produto
$categoriaProduto = $crud->Listar('tbl_produtos', " WHERE categoria_produto LIKE '%$nome_categoria%' ");
if (count($categoriaProduto) > 0) {
  echo "<script>Swal.fire({  
    icon: 'error',
    title: 'Desculpe, a Categoria não pode ser Editada enquanto estiver ligada a um produto',
    showConfirmButton: false,
    timer: 4000
  })</script>";
  echo ' <script>setTimeout(function(){window.location.reload(true)}, 4100);</script>';
  exit();
}

$campos = array(
  'cod_categoria'  => $cod_categoria,
  'nome_categoria' => $nome_categoria,
  'slug_categoria' => $slug_categoria
);
if ($crud->Editar('tbl_categorias', $campos, $chave) != true) {
  echo "<script>Swal.fire({  
    icon: 'error',
    title: 'Categoria não Editada',
    showConfirmButton: false,
    timer: 1500
  })</script>";
  //inserir log
  $campos_log = array(
    'tipo_acao'   => 'update',
    'tbl_acao'    => 'tbl_categorias',
    'status_acao' => 'erro',
    'ip_usuario'  => $ip
  );
  $crud->Insert('tbl_logs', $campos_log);
  echo ' <script>setTimeout(function(){window.location.reload(true)}, 1650);</script>';
} else {
  echo "<script>Swal.fire({  
    icon: 'success',
    title: 'Categoria Editada',
    showConfirmButton: false,
    timer: 1500
  })</script>";

  if (!empty($_FILES['imagem_produto']['name'])) {
    $upload->Send();
  }
  //inserir log
  $campos_log = array(
    'tipo_acao'   => 'update',
    'tbl_acao'    => 'tbl_categorias',
    'status_acao' => 'sucesso',
    'ip_usuario'  => $ip
  );
  $crud->Insert('tbl_logs', $campos_log);
  echo ' <script>setTimeout(function(){window.location.reload(true)}, 1650);</script>';
}
