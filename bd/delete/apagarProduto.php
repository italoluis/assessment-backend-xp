<?php
include '../../classes/class.crud.php';
$crud = new Crud;

$ip = $_SERVER['REMOTE_ADDR'];

$id = $crud->EscapaSql($_POST['id_produto']);
$imagem_produto = $crud->EscapaSql($_POST['img_produto']);

$chave = array(
  'id_produto' => $id
);

if ($crud->Deletar('tbl_produtos', $chave) != true) {
  echo "<script>Swal.fire({  
    icon: 'error',
    title: 'Produto Não Apagado',
    showConfirmButton: false,
    timer: 1500
  })</script>";
  //inserir log
  $campos_log = array(
    'tipo_acao'   => 'delete',
    'tbl_acao'    => 'tbl_produtos',
    'status_acao' => 'erro',
    'ip_usuario'  => $ip
  );
  $crud->Insert('tbl_logs', $campos_log);
  echo ' <script>setTimeout(function(){window.location.reload(true)}, 1650);</script>';
} else {
  echo "<script>Swal.fire({  
    icon: 'success',
    title: 'Produto Apagado',
    showConfirmButton: false,
    timer: 1500
  })</script>";
  unlink("../../upload/$imagem_produto");
  //inserir log
  $campos_log = array(
    'tipo_acao'   => 'delete',
    'tbl_acao'    => 'tbl_produtos',
    'status_acao' => 'sucesso',
    'ip_usuario'  => $ip
  );
  $crud->Insert('tbl_logs', $campos_log);
  echo ' <script>setTimeout(function(){window.location.reload(true)}, 1650);</script>';
}
