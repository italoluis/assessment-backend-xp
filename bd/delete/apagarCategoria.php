<?php
include '../../classes/class.crud.php';
$crud = new Crud;

$ip = $_SERVER['REMOTE_ADDR'];

$id = $crud->EscapaSql($_POST['id_categoria']);
$nome_categoria = $crud->EscapaSql($_POST['nome_categoria']);

//Testar se a categoria esta ligada a algum produto
$categoriaProduto = $crud->Listar('tbl_produtos', " WHERE categoria_produto LIKE '%$nome_categoria%' ");
if (count($categoriaProduto) > 0) {
  echo "<script>Swal.fire({  
    icon: 'error',
    title: 'Desculpe, a Categoria não pode ser Apagada enquanto estiver ligada a um produto',
    showConfirmButton: false,
    timer: 4000
  })</script>"; 
  echo ' <script>setTimeout(function(){window.location.reload(true)}, 4100);</script>';
  exit();
}

$chave = array(
  'id_categoria' => $id
);

if ($crud->Deletar('tbl_categorias', $chave) != true) {
  echo "<script>Swal.fire({  
    icon: 'error',
    title: 'Categoria não Apagada',
    showConfirmButton: false,
    timer: 1500
  })</script>";
   //inserir log
   $campos_log = array(
    'tipo_acao'   => 'delete',
    'tbl_acao'    => 'tbl_categorias',
    'status_acao' => 'erro',
    'ip_usuario'  => $ip
    );
    $crud->Insert('tbl_logs', $campos_log);
  echo ' <script>setTimeout(function(){window.location.reload(true)}, 1650);</script>';
} else {
  echo "<script>Swal.fire({  
    icon: 'success',
    title: 'Categoria Apagada',
    showConfirmButton: false,
    timer: 1500
  })</script>";
   //inserir log
   $campos_log = array(
    'tipo_acao'   => 'delete',
    'tbl_acao'    => 'tbl_categorias',
    'status_acao' => 'sucesso',
    'ip_usuario'  => $ip
    );
    $crud->Insert('tbl_logs', $campos_log);
  echo ' <script>setTimeout(function(){window.location.reload(true)}, 1650);</script>';
}
