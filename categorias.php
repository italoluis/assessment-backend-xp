<?php include 'includes/header.php';?>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Categorias</h1>
      <a href="nova_categoria.php" class="btn-action">Add Nova Categoria</a>
    </div>
    <?php if(count($categorias) >0){?>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Nome</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Codigo</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Ações</span>
        </th>
      </tr> 
      <?php foreach($categorias as $categoria){?>
      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?php echo $categoria['nome_categoria'];?></span>
        </td>      
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?php echo $categoria['cod_categoria'];?></span>
        </td>      
        <td class="data-grid-td">
          <div class="actions">
            <div class="action edit"><a href="editar_categoria.php?id=<?php echo $categoria['id_categoria'];?>"><span>Editar</span></a></div>
            <div class="action delete"><a class="del-cat" data-id-cat="<?php echo $categoria['id_categoria'];?>" data-nome-cat="<?php echo $categoria['nome_categoria'];?>" href="categorias.php?id=<?php echo $categoria['id_categoria'];?>"><span>Apagar</span></a></div>
          </div>
        </td>
      </tr>
      <?php } ?>
    </table>
      <?php }else{?>
        <h3>Ainda sem registros</h3>
      <?php } ?>
    <div id="resposta"></div>
  </main>
  <!-- Main Content -->
  <?php include 'includes/footer.php';?>

  <script>
    $(document).ready(function(){
      $('.del-cat').click(function(){
        event.preventDefault();
         var id_categoria = $(this).attr('data-id-cat');    
         var nome_categoria = $(this).attr('data-nome-cat');       
         var confirma = confirm('Apagar o Categoria?');
         if(confirma){
          $.post('bd/delete/apagarCategoria.php',{id_categoria,nome_categoria},function(data){
              $('#resposta').html(data);
            });
         }else{
           alert('Ação cancelada, Categoria Não apagada')
         }
      });
    });
  </script>
