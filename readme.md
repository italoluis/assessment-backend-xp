# Teste BackEnd php Web Jump
O sistema foi desenvolvido buscando atender os requisitos do teste,
para tanto foi implementado um Crud para cadastrar,editar,apagar produtos e categorias.
Também foi implementado um sistema básico de Upload e Log

# versão do PHP 
Foi utilizado a versão 7.0.32

# Banco de Dados
Foi usado o Mysql (MariaDB) versão 5.7.23

# Como Rodar o sistema
- Dentro da pasta classes, abra o arquivo **classes/db.php** e edite a **linha 7**, informando
o **servidor** ,**usuário**, **senha** e **banco de dados**.
- Importe o arquivo **mysql.sql**

# Estrutura das pastas
**classes** Nessa pasta estão as classes criadas para o sistema.

**bd** Dentro dessa existem outras pastas: **insert** , **update** e **delete**

- Pasta **insert** Nessa pasta estão os arquivos para inserção no banco.

- Pasta **update** Nesse pasta estão os arquivos para update no banco.

- Pasta **delete** nessa pasta estão os arquivos para deletar no banco.

**includes** Essa pasta foi usada apenas para organizar alguns arquivos.

**upload** Recebe as imagens cadastradas.

# Tabelas Criadas
- **tbl_produtos** 
- **tbl_categorias**
- **tbl_logs**

# Plugins Utilizados (Front End)
- [sweet alert 2](https://cdn.jsdelivr.net/npm/sweetalert2@9/)
- [jQuery Form Plugin](http://jquery.malsup.com/form/)


# Contatos
**Email** italo.luis.s@gmail.com

**Whatsapp** 31993346096

**linkedin** (https://www.linkedin.com/in/italoluiz/)

Obrigado pela Oportunidade!
