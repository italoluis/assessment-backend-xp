<?php include 'includes/header.php';?>
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Nova Categoria</h1>
    
    <form name="form" id="form" method="post" action="bd/insert/novaCategoria.php">
      <div class="input-field">
        <label for="category-name" class="label">Nome Categoria</label>
        <input type="text" id="nome_categoria" name="nome_categoria" class="input-text" required/>
        
      </div>
      <div class="input-field">
        <label for="category-code" class="label">Código Categoria</label>
        <input type="text" id="cod_categoria" name="cod_categoria" class="input-text" required />
        
      </div>
      <div class="actions-form">
        <a href="categorias.php" class="action back">Voltar</a>
        <input name="save" class="btn-submit btn-action"  type="submit" value="Salvar" />
      </div>
    </form>
    <div id="resposta"></div>
  </main>
  <!-- Main Content -->
  <?php include 'includes/footer.php';?>
