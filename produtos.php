<?php include 'includes/header.php';?>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Produtos</h1>
      <a href="novo_produto.php" class="btn-action">Add Novo Produto</a>
    </div>
    <?php if(count($produtos) > 0){?>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Nome</span>
        </th>        
        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Preço</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quantidade</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categorias</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Ações</span>
        </th>
      </tr>
     <?php foreach($produtos as $produto){?>
      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?php echo $produto['nome_produto'];?></span>
        </td>      
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?php echo $produto['sku_produto'];?></span>
        </td>
        <td class="data-grid-td">
           <span class="data-grid-cell-content">R$ <?php echo number_format($produto['preco_produto'],2,'.',',');?></span>
        </td>
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?php echo $produto['quantidade_produto'];?></span>
        </td>
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?php
            $json = json_decode($produto['categoria_produto'],true);
            echo $implode = implode(',',$json);?></span>
        </td>      
        <td class="data-grid-td">
          <div class="actions">
            <div class="action edit"><a href="editar_produto.php?id=<?php echo $produto['id_produto'];?>"><span>Editar</span></a></div>
            <div class="action delete"><a class="del-prod" data-id-prod="<?php echo $produto['id_produto'];?>" data-img-prod="<?php echo $produto['imagem_produto'];?>"   href="produtos.php?del=s&id=<?php echo $produto['id_produto'];?>"><span>Apagar</span></a></div>
          </div>
        </td>
      </tr>
     <?php } ?>
    </table>
     <?php } else{?>
      <h3>Ainda sem registros</h3>
     <?php } ?>
    <div id="resposta"></div>
  </main>
  <!-- Main Content -->

  <?php include 'includes/footer.php';?>

  <script>
    $(document).ready(function(){
      $('.del-prod').click(function(){
        event.preventDefault();
         var id_produto  = $(this).attr('data-id-prod');
         var img_produto = $(this).attr('data-img-prod');
         var confirma   = confirm('Apagar o Produto?');
         if(confirma){
            $.post('bd/delete/apagarProduto.php',{id_produto,img_produto},function(data){
              $('#resposta').html(data);
            });
         }else{
           alert('Ação cancelada, Produto Não apagado')
         }
      });
    });
  </script>
