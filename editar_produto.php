<?php include 'includes/header.php';

$id = $_GET['id'];
$chave = array(
  'id_produto' => $id
);
$produto   = $crudRead->ListarSingle('tbl_produtos', $chave);

?>
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Editar Produto</h1>
    
    <form name="form" id="form" method="post" enctype="multipart/form-data" action="bd/update/editarProduto.php">
    <input type="hidden" id="id" name="id" value="<?php echo $produto[0]['id_produto'];?>" /> 
    <div class="input-field">
        <label for="sku_produto" class="label">SKU do Pproduto</label>
        <input type="text" id="sku_produto" name="sku_produto" class="input-text" value="<?php echo $produto[0]['sku_produto'];?>" required /> 
      </div>
      <div class="input-field">
        <label for="nome_produto" class="label">Nome do Produto</label>
        <input type="text" id="nome_produto" name="nome_produto" class="input-text" value="<?php echo $produto[0]['nome_produto'];?>" required /> 
      </div>
      <div class="input-field">
        <label for="imagem_produto" class="label">Imagem do Produto</label>
        <input type="file" id="imagem_produto" name="imagem_produto" class="input-text" /> 
        <input type="hidden" id="imagem_original" name="imagem_original" value="<?php echo $produto[0]['imagem_produto'];?>" /> 
      </div>
      <div class="input-field">
        <label for="preco_produto" class="label">Preço</label>
        <input type="number" step="0.01" id="preco_produto" name="preco_produto" class="input-text" value="<?php echo $produto[0]['preco_produto'];?>" required /> 
      </div>
      <div class="input-field">
        <label for="quantidade_produto" class="label">Quantidade</label>
        <input type="number" min="0" id="quantidade_produto" name="quantidade_produto" class="input-text" value="<?php echo $produto[0]['quantidade_produto'];?>" required /> 
      </div>
      <div class="input-field">
        <label for="categoria_produto" class="label">Categorias</label>
        <select multiple id="categoria_produto" name="categoria_produto[]" class="input-text">        
          <?php
          $json = json_decode($produto[0]['categoria_produto'],true);          
          foreach($categorias as $categoria){
            if(in_array($categoria['nome_categoria'],$json)){
              $status = 'selected';
            }else{
              $status = '';
            }            
          ?>
          <option <?php echo $status;?>><?php echo $categoria['nome_categoria'];?></option>         
          <?php } ?>
        </select>
      </div>
      <div class="input-field">
        <label for="descricao_produto" class="label">Descrição</label>
        <textarea id="descricao_produto" name="descricao_produto" required class="input-text"><?php echo $produto[0]['descricao_produto'];?></textarea>
      </div>
      <div class="actions-form">
        <a href="produtos.php" class="action back">Voltar</a>
        <input class="btn-submit btn-action" name="cad" type="submit" value="Editar Produto" />
      </div>
      
    </form>
  <div id="resposta"></div>
  </main>
  <!-- Main Content -->

<?php include 'includes/footer.php';?>
