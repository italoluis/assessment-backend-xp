<?php include 'includes/header.php';
$qtda_produtos = count($produtos);
if($qtda_produtos >0){
  $msg_topo = '<span>Você possui <b>' . $qtda_produtos . '</b> Produtos Cadastrados na Loja</span>';

}else{
  $msg_topo = '<span>Você não possui produtos cadastrados</span>';
}

?>
  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Dashboard</h1>
    </div>
    <div class="infor">
     <?php echo $msg_topo;?> <a href="novo_produto.php" class="btn-action">Add Produto</a>
    </div>

    <ul class="product-list">

    <?php foreach($produtos as $produto){?>
      <li>
        <div class="product-image">
          <img src="upload/<?php echo $produto['imagem_produto'];?>" layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" />
        </div>
        <div class="product-info">
          <div class="product-name"><span><?php echo $produto['nome_produto'];?></span></div>
          <div class="product-price"><span class="special-price"><?php echo $produto['quantidade_produto'];?> Disponiveis</span> <span>R$ <?php echo number_format($produto['preco_produto'],2,'.',',');?></span></div>
        </div>
      </li>
    <?php } ?>
      
    </ul>
  </main>
  <!-- Main Content -->

  <?php include 'includes/footer.php';?>
