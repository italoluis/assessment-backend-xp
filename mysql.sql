-- --------------------------------------------------------
-- Servidor:                     localhost
-- Versão do servidor:           5.7.23 - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela testeWebjump.tbl_categorias
CREATE TABLE IF NOT EXISTS `tbl_categorias` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `nome_categoria` char(50) DEFAULT NULL,
  `slug_categoria` char(50) DEFAULT NULL,
  `cod_categoria` char(50) DEFAULT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela testeWebjump.tbl_categorias: 7 rows
/*!40000 ALTER TABLE `tbl_categorias` DISABLE KEYS */;
INSERT INTO `tbl_categorias` (`id_categoria`, `nome_categoria`, `slug_categoria`, `cod_categoria`) VALUES
	(16, 'adulto', 'adulto', 'njk125'),
	(15, 'feminino', 'feminino', 'mkl4587'),
	(12, 'Corridas', 'corridas', 'klp123'),
	(14, 'masculino', 'masculino', 'mklj458'),
	(13, 'Caminhadas', 'caminhadas', 'kml1547'),
	(19, 'infantil', 'infantil', 'klo25');
/*!40000 ALTER TABLE `tbl_categorias` ENABLE KEYS */;

-- Copiando estrutura para tabela testeWebjump.tbl_logs
CREATE TABLE IF NOT EXISTS `tbl_logs` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_acao` char(15) DEFAULT NULL,
  `tbl_acao` char(16) DEFAULT NULL,
  `data_acao` datetime DEFAULT CURRENT_TIMESTAMP,
  `status_acao` char(20) DEFAULT NULL,
  `ip_usuario` char(30) DEFAULT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela testeWebjump.tbl_logs: 0 rows
/*!40000 ALTER TABLE `tbl_logs` DISABLE KEYS */;
INSERT INTO `tbl_logs` (`id_log`, `tipo_acao`, `tbl_acao`, `data_acao`, `status_acao`, `ip_usuario`) VALUES
	(1, 'insert', 'tbl_categorias', '2020-05-15 15:40:26', 'sucesso', '::1'),
	(2, 'update', 'tbl_categorias', '2020-05-15 15:40:46', 'sucesso', '::1'),
	(3, 'delete', 'tbl_categorias', '2020-05-15 15:41:07', 'sucesso', '::1'),
	(4, 'insert', 'tbl_produtos', '2020-05-15 15:42:25', 'sucesso', '::1'),
	(5, 'update', 'tbl_produtos', '2020-05-15 15:42:52', 'sucesso', '::1'),
	(6, 'delete', 'tbl_produtos', '2020-05-15 15:43:08', 'sucesso', '::1'),
	(7, 'delete', 'tbl_produtos', '2020-05-15 15:43:33', 'sucesso', '::1'),
	(8, 'update', 'tbl_produtos', '2020-05-15 15:44:58', 'sucesso', '::1'),
	(9, 'update', 'tbl_categorias', '2020-05-15 15:45:08', 'sucesso', '::1'),
	(10, 'update', 'tbl_produtos', '2020-05-15 15:45:22', 'sucesso', '::1'),
	(11, 'insert', 'tbl_produtos', '2020-05-15 15:49:16', 'sucesso', '::1'),
	(12, 'insert', 'tbl_produtos', '2020-05-15 15:52:24', 'sucesso', '::1'),
	(13, 'delete', 'tbl_produtos', '2020-05-15 15:53:09', 'sucesso', '::1'),
	(14, 'insert', 'tbl_produtos', '2020-05-15 15:53:39', 'sucesso', '::1'),
	(15, 'delete', 'tbl_produtos', '2020-05-15 15:57:43', 'sucesso', '::1'),
	(16, 'update', 'tbl_produtos', '2020-05-15 15:58:39', 'sucesso', '::1'),
	(17, 'update', 'tbl_produtos', '2020-05-15 15:59:58', 'sucesso', '::1'),
	(18, 'delete', 'tbl_produtos', '2020-05-15 16:00:23', 'sucesso', '::1'),
	(19, 'delete', 'tbl_categorias', '2020-05-15 16:01:02', 'sucesso', '::1'),
	(20, 'insert', 'tbl_produtos', '2020-05-15 16:02:41', 'sucesso', '::1'),
	(21, 'update', 'tbl_produtos', '2020-05-15 16:03:31', 'sucesso', '::1'),
	(22, 'update', 'tbl_produtos', '2020-05-15 16:04:05', 'sucesso', '::1'),
	(23, 'delete', 'tbl_produtos', '2020-05-15 16:04:15', 'sucesso', '::1'),
	(24, 'insert', 'tbl_categorias', '2020-05-15 16:04:40', 'sucesso', '::1'),
	(25, 'update', 'tbl_categorias', '2020-05-15 16:04:57', 'sucesso', '::1'),
	(26, 'delete', 'tbl_categorias', '2020-05-15 16:05:03', 'sucesso', '::1'),
	(27, 'update', 'tbl_produtos', '2020-05-15 16:06:34', 'sucesso', '::1'),
	(28, 'update', 'tbl_produtos', '2020-05-15 16:07:20', 'sucesso', '::1'),
	(29, 'update', 'tbl_produtos', '2020-05-15 16:07:30', 'sucesso', '::1'),
	(30, 'update', 'tbl_produtos', '2020-05-15 16:07:50', 'sucesso', '::1'),
	(31, 'update', 'tbl_produtos', '2020-05-15 16:08:07', 'sucesso', '::1'),
	(32, 'insert', 'tbl_categorias', '2020-05-15 16:09:13', 'sucesso', '::1'),
	(33, 'update', 'tbl_categorias', '2020-05-15 16:09:26', 'sucesso', '::1'),
	(34, 'delete', 'tbl_categorias', '2020-05-15 16:09:32', 'sucesso', '::1');
/*!40000 ALTER TABLE `tbl_logs` ENABLE KEYS */;

-- Copiando estrutura para tabela testeWebjump.tbl_produtos
CREATE TABLE IF NOT EXISTS `tbl_produtos` (
  `id_produto` int(11) NOT NULL AUTO_INCREMENT,
  `nome_produto` char(160) DEFAULT NULL,
  `slug_produto` char(190) DEFAULT NULL,
  `sku_produto` char(20) DEFAULT NULL,
  `imagem_produto` char(60) DEFAULT NULL,
  `preco_produto` decimal(10,1) DEFAULT NULL,
  `descricao_produto` text,
  `quantidade_produto` int(11) DEFAULT NULL,
  `categoria_produto` text,
  `data_cadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_produto`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela testeWebjump.tbl_produtos: 5 rows
/*!40000 ALTER TABLE `tbl_produtos` DISABLE KEYS */;
INSERT INTO `tbl_produtos` (`id_produto`, `nome_produto`, `slug_produto`, `sku_produto`, `imagem_produto`, `preco_produto`, `descricao_produto`, `quantidade_produto`, `categoria_produto`, `data_cadastro`) VALUES
	(33, 'TÃªnis Runner Bolt', 'tenis-runner-bolt', 'mkl4587', 'vpV18zSJZcTi.png', 325.5, 'Sed eget iaculis urna. Phasellus finibus purus imperdiet tincidunt pharetra. Nunc at neque sit amet nunc pellentesque aliquet vitae ut diam.', 15, '["adulto","masculino","Caminhadas"]', '2020-05-14 17:47:16'),
	(34, 'TÃªnis Basket Light', 'tenis-basket-light', 'njk5698', 'vgFbtkEOLiZd.png', 360.2, 'Phasellus convallis est at nulla finibus, nec consequat nibh semper. In elementum sem elit, eget interdum purus tincidunt varius. Nam et orci enim. Ut vel gravida sem, eget euismod lacus.', 45, '["adulto","feminino","Corridas"]', '2020-05-14 17:48:16'),
	(35, 'TÃªnis 2D Shoes', 'tenis-2d-shoes', 'njm458', 'Er401KgeBH7X.png', 199.2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae turpis vitae urna sollicitudin commodo malesuada eget risus. Pellentesque eu magna at nunc commodo fringilla vitae id justo. Suspendisse vulputate auctor sem, ac fringilla eros eleifend at. Sed posuere leo eget venenatis placerat.', 52, '["infantil","Caminhadas","Corridas"]', '2020-05-14 17:49:12'),
	(36, 'TÃªnis Sneakers 43N', 'tenis-sneakers-43n', 'nmkl1256', 'Xf89nuxwy1B0.png', 236.2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae turpis vitae urna sollicitudin commodo malesuada eget risus. Pellentesque eu magna at nunc commodo fringilla vitae id justo. Suspendisse vulputate auctor sem, ac fringilla eros eleifend at. Sed posuere leo eget venenatis placerat.', 65, '["adulto","feminino","masculino","Caminhadas","Corridas"]', '2020-05-14 17:50:06');
/*!40000 ALTER TABLE `tbl_produtos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
