<?php include 'includes/header.php';?>
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Novo Produto</h1>
    
    <form name="form" id="form" method="post" enctype="multipart/form-data" action="bd/insert/novoProduto.php">
      <div class="input-field">
        <label for="sku_produto" class="label">SKU do Produto</label>
        <input type="text" id="sku_produto" name="sku_produto" class="input-text" required /> 
      </div>
      <div class="input-field">
        <label for="nome_produto" class="label">Nome Produto</label>
        <input type="text" id="nome_produto" name="nome_produto" class="input-text" required /> 
      </div>
      <div class="input-field">
        <label for="imagem_produto" class="label">Imagem do Produto</label>
        <input type="file" id="imagem_produto" name="imagem_produto" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="preco_produto" class="label">Preço</label>
        <input type="number" step="0.01" id="preco_produto" name="preco_produto" class="input-text" required /> 
      </div>
      <div class="input-field">
        <label for="quantidade_produto" class="label">Quantidade</label>
        <input type="number" min="1" id="quantidade_produto" name="quantidade_produto" class="input-text" required /> 
      </div>
      <div class="input-field">
        <label for="categoria_produto" class="label">Categorias</label>
        <select multiple id="categoria_produto" name="categoria_produto[]" class="input-text" required>
          <?php foreach($categorias as $categoria){?>
          <option value="<?php echo $categoria['nome_categoria'];?>"><?php echo $categoria['nome_categoria'];?></option>
          <?php } ?>
        </select>
      </div>
      <div class="input-field">
        <label for="descricao_produto" class="label">Descrição</label>
        <textarea id="descricao_produto" name="descricao_produto" required class="input-text"></textarea>
      </div>
      <div class="actions-form">
        <a href="produtos.php" class="action back">Voltar</a>
        <input class="btn-submit btn-action" name="cad" type="submit" value="Salvar Produto" />
      </div>      
    </form> 
    <div id="resposta"></div>
  </main>
  <!-- Main Content -->

<?php include 'includes/footer.php';?>
