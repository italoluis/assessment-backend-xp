<?php
include 'db.php';

class Crud extends ConexaoBanco
{
  // Escapa o SQL
  public function EscapaSql($trata)
  {
    return mysqli_real_escape_string($this->con, $trata);
  }

  // Cadastra na tabela Informada
  public function Insert($tabela, array $campos)
  {

    $sql = "INSERT " . $tabela . " SET ";
    $campo = '';
    foreach ($campos as $key => $value) {
      $campo .= $this->EscapaSql($key) . ' = ' . "'" .  $this->EscapaSql($value) . "'" . ',';
    }
    $sql .= substr($campo, 0, -1);

    $query = mysqli_query($this->con, $sql);
    if (!$query) {
      return false;
    } else {
      return true;
    }
  }

  // Faz a listagem com todas as informações
  public function Listar($tabela, $raw = false)
  {
    $sql = "SELECT * FROM $tabela $raw";
    $campos = array();
    $query = mysqli_query($this->con, $sql);
    while ($row = mysqli_fetch_array($query)) {
      $campos[] = $row;
    }
    return $campos;
  }

  // Faz a listagem pegando o campo informado como base
  public function ListarSingle($tabela, array $campos)
  {
    $sql = "SELECT * FROM $tabela";
    $sql .= " WHERE ";
    $sql .= implode(",", array_keys($campos));
    $sql .= " = ";
    $sql .= "'" . implode("','", array_values($campos)) . "'";

    $campos = array();
    $query = mysqli_query($this->con, $sql);
    while ($row = mysqli_fetch_assoc($query)) {
      $campos[] = $row;
    }
    return $campos;
  }

  // Edita as informações da tabela baseando na chave informada
  public function Editar($tabela, array $campos, array $chave)
  {
    $sql = "UPDATE $tabela SET ";
    $campo = '';
    foreach ($campos as $key => $value) {
      $campo .= $this->EscapaSql($key) . ' = ' . "'" .  $this->EscapaSql($value) . "'" . ',';
    }
    $sql .= substr($campo, 0, -1);
    $sql .= " WHERE ";
    $sql .= implode(",", array_keys($chave));
    $sql .= " = ";
    $sql .= "'" . implode("','", array_values($chave)) . "'";

    $query = mysqli_query($this->con, $sql);
    if (!$query) {
      return false;
    } else {
      return true;
    }
  }

  // Apaga as informações na tabela baseado na chave passada
  public function Deletar($tabela, array $chave)
  {
    $sql = "DELETE FROM $tabela";
    $sql .= " WHERE ";
    $sql .= implode(",", array_keys($chave));
    $sql .= " = ";
    $sql .= "'" . implode("','", array_values($chave)) . "'";

    $query = mysqli_query($this->con, $sql);
    if (!$query) {
      return false;
    } else {
      return true;
    }
  }
}
