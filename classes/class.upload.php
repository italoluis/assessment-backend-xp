<?php

class Upload
{
  private $arquivo;
  private $caminho;
  private $novoNome;

  function __construct($arquivo, $caminho, $novoNome)
  {
    $this->arquivo = $arquivo;
    $this->caminho = $caminho;
    $this->novoNome = $novoNome;
  }

  private function novoNomeImg()
  {
    if (empty($this->novoNome)) {
      $this->novoNome = uniqid();
    }
    return $novoNomeImg = $this->novoNome;
  }

  public function extensaoImg()
  {
    return $tipoImg = pathinfo($this->nomeImg(), PATHINFO_EXTENSION);
  }

  private function validar($tipoImg)
  {
    $tipos = array('png', 'jpg', 'jpeg', 'gif');
    if (in_array($tipoImg, $tipos)) {
      return true;
    }
  }

  private function nomeImg()
  {
    return $this->arquivo['name'];
  }

  private function caminhoTemp()
  {
    return $this->arquivo['tmp_name'];
  }

  public function Send()
  {
    $novoNome = $this->novoNomeImg();
    $caminho  = $this->caminho . $novoNome . '.' . $this->extensaoImg();

    if ($this->validar($this->extensaoImg())) {

      if (!move_uploaded_file($this->caminhoTemp(), $caminho)) {
        return false;
      } else {
        return true;
      }
    }
  }
}
